<?php
session_start();
include_once("functions.php");
$navbarTitle = "My tickets";

if (!isset($_SESSION["email_login"])) {
  ?><script>window.location.href='login.php?log=unlog'; </script><?php
  exit();
}

$email = $_SESSION["email_login"];
$info = array();
$info = getUserDataByMail($email);
$userId = $info["id"];



?>
 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<title>Product</title>
  <link rel="stylesheet" type="text/css" href="lib/css/style.css">
 	<link rel="stylesheet" type="text/css" href="lib/css/purchased.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="lib/js/functions.js"></script>

 </head>
 <body>
   	<div class="wrapper">
   		<?php include('header.php'); ?>
      <div class="eventlist">
        <div class="subtitle">Upcoming events</div>
        <?php
        $products = getListOfCurrentPurchasedEvents($userId, date("Y-m-d"));
        include("components/purchasedProducts.php");
        ?>

        <div class="subtitle"> Past events </div>
        <?php
        $products = getListOfPastPurchasedEvents($userId, date("Y-m-d"));
        include("components/purchasedProducts.php");
        ?>
      </div>
    </div>
  </body>
</html>
