<?php
include_once("classes/class_order.php");
session_start();
include_once("classes/class_tickets.php");

$order = new order();
if(isset($_GET['order'])){
	if(isset($_SESSION['order'])){
		$order = $_SESSION['order'];
	}
	$ticketIds = json_decode(htmlspecialchars($_GET['order']));
	for ($i=0; $i <count($ticketIds); $i++) {
		if (isset($_POST['availability' .$ticketIds[$i]])) {
			$amount = htmlspecialchars($_POST['availability' .$ticketIds[$i]]);
			if($amount > 0 && $amount<= getTicketAmountById($ticketIds[$i])){
				$order->addCouple($ticketIds[$i], $amount);
			}
		}
	}

	$_SESSION['order'] = $order;
	?><script>window.location.href='cart.php'; </script><?php
}
else{
	?><script>window.location.href='error.php'; </script><?php
}
?>
