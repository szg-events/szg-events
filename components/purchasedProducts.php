<?php
foreach ($products as $pid => $prod) { ?>
  <div class="eventbox">
    <div class="purchasedImg shadow"><img src="images/events/<?php echo $prod->getImage() ?>"></div>
    <div class="purchasedInfo">
      <div class="headline1"><?php echo $prod->getName() ?></div>
      <div>
        <?php echo date("Y-M-d h:i", strtotime($prod->getStartDate())); ?>
        <?php echo $prod->getEndDate() ? " - ".date("Y-M-d h:i", strtotime($prod->getEndDate())) : ""; ?>
      </div>
      <div class="headline2">Purchase details</div>
      <div class="orderSummary shadow">
        <div class="ticketSummary">
          <div class="left title">Ticket</div>
          <div class="right title">#</div>
          <?php
          $total = 0;
          foreach ($prod->getTickets() as $tid => $ticket) { ?>
            <div class="left">
              <?php echo $ticket->getName() ?>
            </div>
            <div class="right">
               <?php echo $ticket->getQuantity() ?>
            </div>
            <?php
            $total = $total + ($ticket->getPrice() * $ticket->getQuantity());
          } ?>
        </div>
        <div class="total">
          <div class="totaltitle">Total of your order</div>
          <div class="totalprice"> € <?php echo $total ?>  </div>
        </div>
      </div>
    </div>
    <div class="dbutton" onclick="location.href='product.php?id=<?php echo $pid ?>'">
      <p>Details</p>
    </div>
  </div>
  <?php
}
?>
