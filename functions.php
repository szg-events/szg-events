<?php
include_once("database.php");
include_once("classes/class_tickets.php");
include_once("classes/class_PurchasedEvent.php");
ini_set('displat_errors', 1);
error_reporting(E_ALL);

function test(){
	global $db;
	$stmt = $db->prepare("SELECT id FROM events");
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($id);

	while($stmt->fetch()) {
	    echo $id;
	}
	$stmt->free_result();
	$stmt->close();
}

function getObjectFromDB($query, $bind){
	global $db;
	$stmt = $db->prepare($query);
	$stmt->bind_param('i', $bind);
	$stmt->execute();
	$result = $stmt->get_result();
	$paramList = $result->fetch_array(MYSQLI_ASSOC); //can be put a guard to check that $paramList has only one line
	//close connection
	$stmt->free_result();
	$stmt->close();
	return $paramList;
}

function getTicketsFromDB($query, $bind){
	global $db;
	$stmt = $db->prepare($query);
	$stmt->bind_param('i', $bind);
	$stmt->execute();
	$result = $stmt->get_result();
	$paramList = array();
	$i = 0;
	while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
		$paramList[$i++] = $row;
			}
	$stmt->free_result();
	$stmt->close();
	return $paramList;
}

function getTicketAmountById($id){
	global $db;
	$avail_amo = 0;
	$stmt = $db->prepare("SELECT available_amount FROM tickets WHERE id = ? LIMIT 1");
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($avail);
	$stmt->fetch();
	if($stmt->num_rows == 1) {
	    $avail_amo = $avail;
	}
	$stmt->free_result();
	$stmt->close();
	return $avail_amo;
}

function ticketExistsById($id){
  global $db;
  $exists = false;
  $stmt = $db->prepare("SELECT id FROM tickets WHERE id = ? LIMIT 1");
  $stmt->bind_param('i', $id);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($id);
  $stmt->fetch();
  if($stmt->num_rows == 1)
  {
    $exists = true;
  }
  $stmt->free_result();
  $stmt->close();

  return $exists;
}
/**
* Returns only one row with id, firstname and lastname of the user, that is currently logged in.
*/
function getUserDataByMail($email) {
	$query = "SELECT * FROM users WHERE email=? LIMIT 1";
	global $db;
	$stmt = $db->prepare($query);
	$stmt->bind_param('s', $email);
	$stmt->execute();
	$result = $stmt->get_result();
	$paramList = array();
	$i = 0;
	$paramList = $result->fetch_array(MYSQLI_ASSOC);
	//close connection
	$stmt->free_result();
	$stmt->close();
	return $paramList;
}

function getUserDataById($id) {
	$query = "SELECT * FROM users WHERE id=? LIMIT 1";
	global $db;
	$stmt = $db->prepare($query);
	$stmt->bind_param('s', $id);
	$stmt->execute();
	$result = $stmt->get_result();
	$paramList = array();
	$i = 0;
	$paramList = $result->fetch_array(MYSQLI_ASSOC);
	//close connection
	$stmt->free_result();
	$stmt->close();
	return $paramList;
}

function ticketIdsToString($tickets){
	$str = "";
	foreach ($tickets as $key => $tckt) {
		$str .= "position " .$key. ": " .$tckt->getId(). "<br>";
	}
	echo $str;
}

function orderTickets($order, $tickets){
	$newTickets = array();
	for ($i=0; $i < count($order->getOrderRows()); $i++) {
		$id = $order->getTicketFromIndex($i)[0];
			$found = false;
			$j = 0;
			while($found == false && $j<count($tickets)){
				if($tickets[$j]->getId() == $id){
					array_push($newTickets, $tickets[$j]);
					$found = true;
				}
				$j++;
			}
	}
	return $newTickets;
}
/**
* Returns a true if a tuple was successfully updated.
*/
function setUserDataByMail($email, $paramList) {
	$query = "UPDATE users SET ";
	$first = true;
	$bindParams = array();
	$types = "";
	foreach ($paramList as $key => $value) {
		if ($first) {
			$query .= $key."=?";
			$first = false;
		} else {
			$query .= ", ".$key."=?";
		}
		if (is_int($value)) {
			$types .= "i";
		} else {
			$types .= "s";
		}
		array_push($bindParams, $value);
	}
	$query .= " WHERE email=?";
	global $db;
	$stmt = $db->prepare($query);

	$types .= "s";
	array_push($bindParams, $email);
	array_splice($bindParams, 0, 0, $types);
	call_user_func_array(array($stmt, 'bind_param'), refValues($bindParams));

	$stmt->execute();
	$result = false;
	if(mysqli_affected_rows($db) == 1){
		$result = true;
	}
	$stmt->free_result();
	$stmt->close();
	return $result;
}

function setNewPassword($email, $hashedPass) {

	$query = "UPDATE users SET password=? WHERE email=?";
	global $db;
	$stmt = $db->prepare($query);
	$stmt->bind_param('ss', $hashedPass, $email);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($db) == 1){
		$result = true;
	}
	$stmt->free_result();
	$stmt->close();
	return $result;
}

// source: https://stackoverflow.com/questions/16120822/mysqli-bind-param-expected-to-be-a-reference-value-given
function refValues($arr){
    if (strnatcmp(phpversion(),'5.3') >= 0) //Reference is required for PHP 5.3+
    {
        $refs = array();
        foreach($arr as $key => $value)
            $refs[$key] = &$arr[$key];
        return $refs;
    }
    return $arr;
}

/* -- Check User Info -- */

function checkUser($email){

	global $db;
	$stmt = $db->prepare("SELECT * FROM users WHERE email=?");
	$stmt->bind_param("s", $email);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();

	return $result;

}

function getUserDataByEmail($email) {

	global $db;
	$stmt = $db->prepare("SELECT * FROM users WHERE email=?");
	$stmt->bind_param('s', $email);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function createNewUserData($email, $firstname, $lastname, $password){

	global $db;
	$stmt = $db->prepare("INSERT INTO `users` (`email`, `firstname`, `lastname`, `password`) VALUES (?,?,?,?)");
	$stmt->bind_param('ssss', $email, $firstname, $lastname, $password);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($db) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function insertResetId($email, $resetid, $timestamp){

	global $db;
	$stmt = $db->prepare("INSERT INTO `passchange` (`email`, `resetid`, `gen_time`) VALUES (?,?,?)");
	$stmt->bind_param('sss', $email, $resetid, $timestamp);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($db) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function selectResetId($resetid){

	global $db;
	$stmt = $db->prepare("SELECT * FROM passchange WHERE resetid =? ORDER BY ID DESC LIMIT 1");
	$stmt->bind_param('s', $resetid);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;

}


function deleteResetId($resetid){

	global $db;
	$stmt = $db->prepare("DELETE FROM passchange WHERE resetid =?");
	$stmt->bind_param('s', $resetid);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function deleteResetIdByMail($email){

	global $db;
	$stmt = $db->prepare("DELETE FROM passchange WHERE email =?");
	$stmt->bind_param('s', $email);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function deleteOldResetId(){

	global $db;
	$stmt = $db->prepare("DELETE FROM passchange WHERE gen_time < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 60 MINUTE))");
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function updateLogcount($logcount, $email){

	global $db;
	$stmt = $db->prepare("UPDATE users SET logcount=? where email=?");
	$stmt->bind_param('ss', $logcount, $email);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($db) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;

}

function updateactid($actid, $email){

	global $db;
	$stmt = $db->prepare("UPDATE users SET actid=? where email=?");
	$stmt->bind_param('ss', $actid, $email);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($db) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;

}


function insertactid($actid, $email){

	global $db;
	$stmt = $db->prepare("UPDATE users SET actid=? WHERE email=?");
	$stmt->bind_param('ss', $actid, $email);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($db) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;
}


function getListOfPurchasedEvents($userId, $date, $pastEvents) {
		global $db;
		$purchasedEvents = array();
		$query = "SELECT events.id, events.name, events.startdate, events.enddate, events.image, tickets.id, tickets.name, tickets.price_eur, order_details.amount
													FROM orders INNER JOIN order_details on orders.id = order_details.order_id
																			INNER JOIN tickets on order_details.ticket_id = tickets.id
																			INNER JOIN events on tickets.event_id = events.id
													WHERE orders.user_id=?";


		if ($date == null) {
			$query .= " ORDER BY events.startdate DESC";
		} else {
			if ($pastEvents == true) {
				$query .= " AND events.startdate<? ORDER BY events.startdate DESC";
			} else {
				$query .= " AND events.startdate>=? ORDER BY events.startdate ASC";
			}
		}
		$stmt = $db->prepare($query);
		if ($date == null) {
			$stmt->bind_param('i', $userId);
		} else {
			$stmt->bind_param('is', $userId, $date);
		}
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($eventId, $eventName, $startdate, $enddate, $image, $ticketId, $ticketName, $price_eur, $quantity);
		while($stmt->fetch()) {
			if (!isset($purchasedEvents[$eventId])) {
				$event = PurchasedEvent::withValues($eventId, $eventName, $startdate, $enddate, $image);
				$purchasedEvents[$eventId] = $event;
			} else {
				 $event = $purchasedEvents[$eventId];
			}
			$event->addPurchasedTicket($ticketId, $ticketName, $price_eur, $quantity);
		}
		$stmt->free_result();
		$stmt->close();
		return $purchasedEvents;
}

function getListOfAllPurchasedEvents($userId) {
	return getListOfPurchasedEvents($userId, null, null);
}

function getListOfPastPurchasedEvents($userId, $date) {
	return getListOfPurchasedEvents($userId, $date, true);
}

function getListOfCurrentPurchasedEvents($userId, $date) {
	return getListOfPurchasedEvents($userId, $date, false);
}

function generateRandomString() {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	$length = 50;
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	return $randomString;
}

function createOrderFromCart($userId, $orderRows) {
  global $db;
  $query = "INSERT INTO orders (user_id) VALUES (?)";
  $stmt = $db->prepare($query);
  $stmt->bind_param('i', $userId);
  $stmt->execute();
  $orderId = $db->insert_id;
	$stmt->close();

  if ($orderId > 0) {
		foreach ($orderRows as $orderPos) {
      $query = "UPDATE tickets SET available_amount=? WHERE id=?";
			$ticketId = $orderPos[0];
			$amount = $orderPos[1];
      $stmt = $db->prepare($query);
			$available_amount = getTicketAmountById($ticketId);
			if($available_amount < $amount) {
				$amount = $available_amount;
				$_SESSION['errorAmount'] = "The chosen amount is higher then the
				available amount. The orderer amount has been set to the maximum available amount.";
			}
      $newStock = $available_amount - $amount;
      $stmt->bind_param('ii', $newStock, $ticketId);
      $stmt->execute();
      $stmt->close();
      $query = "INSERT INTO order_details (order_id, ticket_id, amount) VALUES (?,?,?)";
      $stmt = $db->prepare($query);
      $stmt->bind_param('iii', $orderId, $ticketId, $amount);
      $stmt->execute();
    	$stmt->close();
    }
  }
	return $orderId;
}

function getDevicesList($mail) {
	global $db;
	$devicesArr = array();
	$query = "SELECT id, identifier, last_login, blocked FROM devices
						WHERE user_id=(SELECT id FROM users WHERE email=? LIMIT 1)
						ORDER BY last_login";
	$stmt = $db->prepare($query);
	$stmt->bind_param('s', $mail);
	$stmt->execute();
	$result = $stmt->get_result();
	$i = 0;
	while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
		$devicesArr[$i++] = $row;
	}
	$stmt->free_result();
	$stmt->close();
	return $devicesArr;
}

?>
