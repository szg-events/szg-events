<?php /*check if the order elements are in the db and are aviailable, then remove them from availability and cerate the order*/

include_once("classes/class_order.php");
include_once("classes/class_tickets.php");

session_start();

$order = new order();
if(isset($_GET['tickets'])){
	if(isset($_SESSION['order'])){
		$order = $_SESSION['order'];
	}
	$ticketIds = json_decode(htmlspecialchars($_GET['tickets']));

	for ($i=0; $i <count($ticketIds); $i++) {
		if (isset($_POST['availability' .$ticketIds[$i]])) {
			$amount = htmlspecialchars($_POST['availability' .$ticketIds[$i]]);
			if($amount > 0 && $amount<= getTicketAmountById($ticketIds[$i])){
				$order->addCouple($ticketIds[$i], $amount);
			}
		}
	}
	$_SESSION['order'] = $order;
	header('location: cart.php');
}
else{
	header('location : error.php');
}
?>
