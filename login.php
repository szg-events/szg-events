<?php
session_start();
include_once("database.php");
include_once("functions.php");
include_once("classes/class_user_access.php");

if(!isset($_SESSION["email_login"])){
	if (isset($_POST["email_login"]) && isset($_POST["password_login"])) {
		$email_login = filter_var($_POST["email_login"], FILTER_SANITIZE_STRING);

		$sql = checkUser($email_login);
		$znrow = mysqli_num_rows($sql);

		if($znrow == 1){
			$zrow = mysqli_fetch_assoc($sql);
			$z_password_sha = $zrow['password'];
			$z_actid = $zrow['actid'];
			$logcount = ++$zrow['logcount'];
			if(isset($_GET['actid'])){
				if($z_actid == $_GET['actid'] && $_GET['actid'] == $z_actid){
					$rw = updateLogcount(0, $email_login);
					$rwx = updateactid("", $email_login);

					if($rw && $rwx){
						$logcount = 1;
					}
				} else {
					if($z_actid != "" || $z_actid != NULL){
						// set flag to block user
						$logcount = 999;
					    echo 'You have made 3 failed login attempts since your last login. Your login privileges are blocked temporarily!<br/>';
					    echo 'Please activate your account from the email sent to you!';
					}
				}
			}
		}
		else{
			//email not found
			$logcount = 999;
			?><script>window.location.href='login.php?log=failedlogin'; </script><?php
		}
		if($logcount < 4){

			$password_login = $_POST["password_login"];
			$password_login_sha = openssl_digest($password_login, 'sha512');

			$sql = checkUser($email_login);
			$zrow = mysqli_fetch_assoc($sql);
			$z_password_sha = $zrow['password'];

			$userCount = mysqli_num_rows($sql);
			if($userCount == 1) {
				while ($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)) {
					$id = $row["id"];
				}

				if($password_login_sha == $z_password_sha){

						$rx = updateLogcount(0, $email_login);
						$_SESSION["email_login"] = $email_login;
						deleteResetIdByMail($email_login);
						$row2 = getUserDataByMail($email_login);

						$thisAccess = new UserAccess($email_login);
						if ($thisAccess->isNewAccess()) {
							$thisAccess->sendEmailToUser();
						}
						$redirect = "index.php?log=success";

						if(isset($_GET['active'])){
							if($_GET['active'] == 'cart'){
								$redirect = "cart.php";
							}
						}
				}
				else{
					$rx = updateLogcount($logcount, $email_login);
					$redirect = 'login.php?log=failedlogin';
					?><script>window.location.href='login.php?log=failedlogin'; </script><?php
				}

				if (isset($_SESSION["redirect"])) {
					$redirect = $_SESSION["redirect"];
					unset($_SESSION["redirect"]);
				}
				?><script>window.location.href='<?php echo $redirect ?>'; </script><?php
				exit();
			} else {
				?><script>window.location.href='login.php?log=failedlogin'; </script><?php
			}


		}
		else if($logcount == 4){
			$configMail = parse_ini_file('../config.ini');
			$address = "https://ec2-54-213-248-219.us-west-2.compute.amazonaws.com"; //Website name or localhost address

			$to = $email_login;
			$subject = "Failed Login Attempts";
			$txt = "Your login privileges are blocked temporarily! Please click on the link below to reactivate your login, and please attempt login with the right credentials.\n
				".$address."/login.php?actid=".$actid."&&change=yes\n
			However, if you have forgotten your password. Please request for a new password by clicking on the link below!\n
				".$address."/forgot_pass.php";
			$headers = "From: ".$configMail['mail']."\r\n";

			if(mail($to,$subject,$txt,$headers)) {
				echo '<script>alert("You have received mail!");</script>';
			}
			else {
				echo '<script>alert("The mail cannot be sent");</script>';
			}
			if (isset($_SESSION["redirect"])) {
				$redirect = $_SESSION["redirect"];
				unset($_SESSION["redirect"]);
			}
		}
	}
}

?><?php

$reg= @$_POST['reg'];
$fn="";
$ln="";
$em="";
$pswd="";
$pswd2="";
$sign_mess ="";
$logcount = 0;
$actid = 0;

$fn= strip_tags(htmlspecialchars(@$_POST['fname']));
$ln= strip_tags(htmlspecialchars(@$_POST['lname']));
$em= strip_tags(htmlspecialchars(@$_POST['email']));
$pswd= strip_tags(htmlspecialchars(@$_POST['password']));
$pswd2= strip_tags(htmlspecialchars(@$_POST['password2']));


if ($reg){
		if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])):      //your site secret key
	     $secret = '6LfACiUUAAAAAI3y3WzXzmdox8P-PGs_O527dXNR';
	     //get verify response data
	     $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
	     $responseData = json_decode($verifyResponse);
	     if($responseData->success):
	     else:
	         $errMsg = 'Robot verification failed, please try again.';
	     endif;
		   else:
		      $errMsg = 'Please click on the reCAPTCHA box.';
		 	  $em ="";
		 	  ?><script>window.location.href='login.php?show=signup&&msg=click'</script><?php
		  	  exit();
		  endif;

	if($em) {
		$em_res = getUserDataByEmail($em);
		$emcheck = mysqli_num_rows($em_res);

		if($emcheck == 0){
			if($em&&$pswd){
				if($pswd){

					if(strlen($fn)>25||strlen($ln)>25){
						$sign_mess = 'input limit is 25 characters';
					}
					else if(strlen($pswd)<8) {
							$sign_mess = 'Your password must be at least 8 characters';
						}
						else {

							$email = filter_var($em, FILTER_SANITIZE_EMAIL);

							if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
							    $sign_mess="Please enter a valid email address";
							}

							if ($pswd != $pswd2){
								$sign_mess="Sorry! Passwords do not match!";
							}

							?>
							<div style="display: none;">
							<form id="dateForm" action="" method="POST">
    						<input id="log-bar" class="login" type="text" name="email_login" size="25" placeholder="Email" value="<?php echo $em; ?>">
    						<input id="log-bar" class="login" type="password" name="password_login" size="25" placeholder="Password" value="<?php echo $pswd; ?>">
	  						<input id="login-but" type="submit" name="login" value="LOGIN">
	  						</form>
	  						</div>
							<?php

							if($sign_mess==""){

								$pswd = openssl_digest($pswd, 'sha512');
								$query= createNewUserData($em, $fn, $ln, $pswd);

								if($query){}else{printf("Error: The Registration was incomplete! %s\n\n", mysqli_error($db));}

								?>
								<script>document.getElementById('dateForm').submit();</script>
								<?php
								}
						}
				}
			}else{$sign_mess =  'Please fill in all the details!';}
		}else{$sign_mess =  'Account w/ email already exists!';}
	}
}

if(isset($_GET['log'])){
	if($_GET['log'] == "unlog"){
		$sign_mess = 'You must log in to continue!';
	}
}

if(isset($_GET['source'])){
	if($_GET['source'] == 'checkout'){
		$sign_mess = 'Please login to complete the purchase';
	}
}

if(isset($_GET['show'])){
	if($_GET['show'] == 'signup'){
		if(isset($_GET['msg'])){
			if($_GET['msg'] == 'click'){
				$sign_mess = 'Please click on the reCAPTCHA';
			}
			else if($errMsg != ""){
				$sign_mess = 'Robot verification failed, please try again.';
			}
		}
	}
}

?>


<head>
	<title>SzG Events | South Tyrol</title>
	<link rel="stylesheet" type="text/css" href="lib/css/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

	<script>
		function showreg(){
			document.getElementById('logdiv').style.display = 'none';
			document.getElementById('regdiv').style.display = 'block';
		};

		function showlog(){
			document.getElementById('regdiv').style.display = 'none';
			document.getElementById('logdiv').style.display = 'block';
		};
	</script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
	<div class="wrapper">
		<?php include ("header.php"); ?>

		<div style="margin: 20px auto; margin-top: 50px; width: 400px; text-align: center;">

			<?php if($sign_mess != ""){ ?>
				<div style="padding: 5px; font-size: 12px; background: whitesmoke; color:grey; width: 240px; margin: 10px auto 30px;">
					<?php echo $sign_mess; ?>
				</div>
			<?php } ?>

			<div id="logdiv">
				<form action="" method="POST">
					<input type="email" name="email_login" placeholder="email"><br/>
					<input type="password" name="password_login" placeholder="password"><br/>
					<input type="submit" name="submit">
				</form>
				<p id="raisefp"><a href="forgot_pass.php">Forgot password?</a></p>
				<p id="raise">Need an account? <span onclick="showreg()" style="cursor: pointer; color: royalblue;">Signup</span></p>
			</div>

			<div id="regdiv">
				<form action="" method="POST" autocomplete="off">
					<input type="text" name="fname" placeholder="Firstname" required><br/>
					<input type="text" name="lname" placeholder="Lastname" required><br/>
					<input type="email" name="email" class="email" placeholder="Email" required><br/>
							<div class="email-yes">Hmm, looks like a valid email!</div>
							<div class="email-no">Please enter a valid email</div>
					<input type="password" name="password" class="password" placeholder="Password" autocomplete="new-password" required><br/>
						    <ul class="helper-text">
						        <li class="length">Must be at least 8 characters long.</li>
						        <li class="lowercase">Must contain a lowercase letter.</li>
						        <li class="uppercase">Must contain an uppercase letter.</li>
						        <li class="special">Must contain a special character.</li>
						        <li class="number">Must contain a number.</li>
						    </ul>
					<input type="password" name="password2" class="password2" placeholder="Re-enter password" required><br/>
							<div class="pass-yes">Yes! Your passwords Match!</div>
							<div class="pass-no">Passwords do not match!</div>
							<div class="g-recaptcha" data-sitekey="6LfACiUUAAAAAMlWnU3aC54gafP-3x9a8LqMPldu" style="margin: 10px 0px;">
							</div>

					<input id="reginp" type="submit" disabled="disabled" name="reg">

				</form>
				<p id="raise">Have an account? <span onclick="showlog()" style="cursor: pointer; color: royalblue;">Login</span></p>
			</div>
		</div>
	</div>

	<script>

		(function(){

			var email = document.querySelector('.email');
			var password = document.querySelector('.password');
			var password2 = document.querySelector('.password2');

			var helperText = {
			    charLength: document.querySelector('.helper-text .length'),
			    lowercase: document.querySelector('.helper-text .lowercase'),
			    uppercase: document.querySelector('.helper-text .uppercase'),
			    special: document.querySelector('.helper-text .special'),
			    number: document.querySelector('.helper-text .number')
			};

			var pattern = {
		    charLength: function() {
		        if( password.value.length >= 8 ) {
		            return true;
		        }
		    },
		    lowercase: function() {
		        var regex = /^(?=.*[a-z]).+$/;

		        if( regex.test(password.value) ) {
		            return true;
		        }
		    },
		    uppercase: function() {
		        var regex = /^(?=.*[A-Z]).+$/;

		        if( regex.test(password.value) ) {
		            return true;
		        }
		    },
		    special: function() {
		        var regex = /^(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]).+$/;

		        if( regex.test(password.value) ) {
		            return true;
		        }
		    },
		    number: function() {
		        var regex = /^(?=.*[0-9]).+$/;

		        if( regex.test(password.value) ) {
		            return true;
		        }
		    }
			};

			function patternTest(pattern, response) {
			    if(pattern) {
			        addClass(response, 'valid');
			    } else {
			        removeClass(response, 'valid');
			    }
			}

			function addClass(el, className) {
			    if (el.classList) {
			        el.classList.add(className);
			    } else {
			        el.className += ' ' + className;
			    }
			}

			function removeClass(el, className) {
			    if (el.classList) {
			        el.classList.remove(className);
			    } else {
			        el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
			    }
			}

			function hasClass(el, className) {
			    if (el.classList) {
			        console.log(el.classList);
			        return el.classList.contains(className);
			    } else {
			        new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
			    }
			}

			password.addEventListener('click', function (){
				document.querySelector(".helper-text").style.display = "block";
			});

			password.addEventListener('keyup', function (){
				patternTest( pattern.charLength(), helperText.charLength );
				patternTest( pattern.lowercase(), helperText.lowercase );
				patternTest( pattern.uppercase(), helperText.uppercase );
				patternTest( pattern.special(), helperText.special );
				patternTest( pattern.number(), helperText.number );
				validatesub();
			});



			password2.addEventListener('keyup', function (){
				validatesub();
			});

			email.addEventListener('keyup', function (){
        		validatesub();
		    });

			function validatesub(){

				var regex = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

				if(password.value == password2.value){
					if(password.value != ""){
						document.querySelector(".pass-no").style.display = "none";
						document.querySelector(".pass-yes").style.display = "block";
					}
					else{
						document.querySelector(".pass-no").style.display = "none";
						document.querySelector(".pass-yes").style.display = "none";
					}

					if( regex.test(email.value) ) {

						document.querySelector(".email-no").style.display = "none";
						document.querySelector(".email-yes").style.display = "block";

						if( hasClass(helperText.charLength, 'valid') &&
					         hasClass(helperText.lowercase, 'valid') &&
					         hasClass(helperText.uppercase, 'valid') &&
					         hasClass(helperText.special, 'valid')) {

								// and if recaptcha is clicked
									$("#reginp").prop( "disabled", false );
									$("#reginp").css("cursor", "pointer");
						}
					}
					else{
						document.querySelector(".email-yes").style.display = "none";
						document.querySelector(".email-no").style.display = "block";

						$("#reginp").prop( "disabled", true );
						$("#reginp").css("cursor", "not-allowed");
					}
				}
				else{
					document.querySelector(".pass-yes").style.display = "none";
					document.querySelector(".pass-no").style.display = "block";

					$("#reginp").prop( "disabled", true );
					$("#reginp").css("cursor", "not-allowed");
				}
			}

		})();
	</script>

	<script type="text/javascript">
	    $(document).ready(function(){
	      $('input[name=password], input[name=password2]').on('keyup', function () {
	        var password   = $('input[name=password]'),
	            repassword = $('input[name=password2]'),
	            both       = password.add(repassword).removeClass('has-success has-error');
	        if (password.val().length > 0){
	          repassword.addClass(
	              password.val().length > 0 ? 'has-success' : 'has-error'
	          );

	          if (password.val() != repassword.val()) {
	              repassword.addClass('has-error');
	          } else {
	            both.addClass('has-success')
	          }
	        }
	      })
	    });
	</script>

	<?php
	if(isset($_GET['show'])){
		if($_GET['show'] == 'signup'){
			$sign_mess = 'Please click on the reCAPTCHA';
			echo '<script type="text/javascript">','showreg();','</script>';
		}
	}
	?>
</body>
