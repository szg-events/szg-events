<?php
include_once("functions.php");
include_once("classes/class_order.php");
session_start();
ini_set('date.timezone', 'Europe/Berlin');
ini_set('display_errors', 1);
error_reporting(E_ALL);

if(isset($_GET['prod']) and isset($_SESSION['order'])){
      $id = htmlspecialchars($_GET['prod']);
      $order = new order();
      $order = $_SESSION['order'];
      if(ticketExistsById($id)){
        $order->removeCouple($id);
        $_SESSION['order'] = $order;
        $order->toString();
      }
}
?>
