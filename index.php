<?php
session_start();
include_once("functions.php");
?>

<html>
<head>
	<title>SzG Events | South Tyrol</title>
	<link rel="stylesheet" type="text/css" href="lib/css/style.css">
</head>
<body>
	<div class="wrapper">
		<?php include('header.php'); ?>

		<div class="eventlist">
		<?php
			$result = mysqli_query($db,"SELECT * FROM events");
			while($row=mysqli_fetch_assoc($result)){
			?>
			<div class="eventbox">
				<div class="eimg" onclick="location.href='product.php?id=<?php echo $row['id'] ?>'">
					<img src="images/events/<?php echo $row['image'] ?>" style="max-width: 100%;">
				</div>
				<div class="edesc">
					<div class="block1">
						<p>Event: <?php echo $row['name']; ?></p>
						<p>Start Date: <?php echo $row['startdate']; ?></p>
					</div>
					<div class="block2">
						<div class="dbutton" onclick="location.href='product.php?id=<?php echo $row['id'] ?>'">
							<p>Details</p>
						</div>
					</div>
				</div>
			</div>
			<?php
			}
		?>
		</div>

		<?php include('footer.php') ?>
	</div>
</body>
</html>
