<?php
session_start();
if (!isset($_SESSION["email_login"])) {
  ?><script>window.location.href='login.php?log=unlog'; </script><?php

  exit();
}

include_once("functions.php");
?>

<?php

$email;
$userId;
$firstName;
$lastName;
$infoMsg = "";

if(isset($_SESSION["email_login"])) {
  $email = $_SESSION["email_login"];

  if (isset($_POST['changeUserInfo'])) {
    $params = array();
    $params["firstname"] = htmlspecialchars($_POST["firstname"]);
    $params["lastname"] = htmlspecialchars($_POST["lastname"]);
    $updatedUserInfo = setUserDataByMail($email, $params);
  }

  $userData = getUserDataByMail($email);
  $userId = $userData["id"];
  $firstName = $userData["firstname"];
  $lastName = $userData["lastname"];


  if (isset($_POST["changePassword"])
      && isset($_POST["oldPass"])
      && isset($_POST["newPass"])
      && isset($_POST["rePass"]))
  {

    $chk = getUserDataByMail($email);
    $old_pass_db = $chk['password'];
    $old_pass_inp = openssl_digest($_POST["oldPass"], 'sha512');
    $new_pass_inp = openssl_digest($_POST["newPass"], 'sha512');

    if($old_pass_db == $old_pass_inp){
      $updatedUserInfo = setNewPassword($email, htmlspecialchars($new_pass_inp));
      $infoMsg="Password changed successfully!";
    }
    else{
      $infoMsg="Old password doesnt match!";
    }
  }

}
else {
  header("Location: login.php");
  exit();
}
 ?>

<head>
  <meta charset="utf-8">
  <title>szg - settings</title>
  <link rel="stylesheet" type="text/css" href="lib/css/style.css">
  <link rel="stylesheet" type="text/css" href="lib/css/animate.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="lib/js/checkPasswords.js"></script>

</head>
<body>
	<?php include("header.php"); ?>

  <div class="mainContainer settings" style="font-size: 14px;">
    <form class="masterForm userInfo" action="" method="POST">
      Name:<br>
      <input type="text" name="firstname" value="<?php echo $firstName ?>">
      <br>
      Lastname:<br>
      <input type="text" name="lastname" value="<?php echo $lastName ?>">
      <br><br>
      <input type="submit" name="changeUserInfo" value="Change personal information">
      <div class="btnCancel" onclick="javascript:window.location='settings.php';">Cancel</div>
    </form>
    <br>


    <form class="masterForm passwordChange" action="" method="post">
      <?php
        if($infoMsg != ""){
          ?>
          <div class='mbox animated bounce'><?php echo $infoMsg; ?></div>
          <?php
        }
      ?>
      <br/>
      Old password:<br>
      <input type="password" name="oldPass" value="" placeholder="old password">
      <br>
      New password:<br>
      <input type="password" class="oldpass" name="newPass" placeholder="new password">
                <ul class="helper-text">
                    <li class="length">Must be at least 8 characters long.</li>
                    <li class="lowercase">Must contain a lowercase letter.</li>
                    <li class="uppercase">Must contain an uppercase letter.</li>
                    <li class="special">Must contain a special character.</li>
                    <li class="number">Must contain a number.</li>
                </ul>
      <br>
      Confirm your new password:<br>
      <input type="password" class="newpass" name="rePass" placeholder="new password">
              <div class="pass-yes">Yes! Your passwords Match!</div>
              <div class="pass-no">Passwords do not match!</div>
      <br><br>
      <input id="reginpx" type="submit" disabled="disabled" name="changePassword" value="Change password">
      <div class="btnCancel" onclick="javascript:window.location='settings.php';">Cancel</div>
    </form>
  </div>

  <script>
      (function(){

        var password = document.querySelector('.oldpass');
        var password2 = document.querySelector('.newpass');

        var helperText = {
            charLength: document.querySelector('.helper-text .length'),
            lowercase: document.querySelector('.helper-text .lowercase'),
            uppercase: document.querySelector('.helper-text .uppercase'),
            special: document.querySelector('.helper-text .special'),
            number: document.querySelector('.helper-text .number')
        };

        var pattern = {
          charLength: function() {
              if( password.value.length >= 8 ) {
                  return true;
              }
          },
          lowercase: function() {
              var regex = /^(?=.*[a-z]).+$/;

              if( regex.test(password.value) ) {
                  return true;
              }
          },
          uppercase: function() {
              var regex = /^(?=.*[A-Z]).+$/;

              if( regex.test(password.value) ) {
                  return true;
              }
          },
          special: function() {
              var regex = /^(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]).+$/;

              if( regex.test(password.value) ) {
                  return true;
              }
          },
          number: function() {
              var regex = /^(?=.*[0-9]).+$/;

              if( regex.test(password.value) ) {
                  return true;
              }
          }
        };

        function patternTest(pattern, response) {
            if(pattern) {
                addClass(response, 'valid');
            } else {
                removeClass(response, 'valid');
            }
        }

        function addClass(el, className) {
            if (el.classList) {
                el.classList.add(className);
            } else {
                el.className += ' ' + className;
            }
        }

        function removeClass(el, className) {
            if (el.classList) {
                el.classList.remove(className);
            } else {
                el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
            }
        }

        function hasClass(el, className) {
            if (el.classList) {
                console.log(el.classList);
                return el.classList.contains(className);
            } else {
                new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
            }
        }

        password.addEventListener('click', function (){
          document.querySelector(".helper-text").style.display = "block";
        });

        password.addEventListener('keyup', function (){
          patternTest( pattern.charLength(), helperText.charLength );
          patternTest( pattern.lowercase(), helperText.lowercase );
          patternTest( pattern.uppercase(), helperText.uppercase );
          patternTest( pattern.special(), helperText.special );
          patternTest( pattern.number(), helperText.number );
          validatesub();
        });

        password2.addEventListener('keyup', function (){
          validatesub();
        });
        //
        // email.addEventListener('keyup', function (){
        //       validatesub();
        //   });

        function validatesub(){

          var regex = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

          if(password.value == password2.value){
            if(password.value != ""){
              document.querySelector(".pass-no").style.display = "none";
              document.querySelector(".pass-yes").style.display = "block";
            }
            else{
              document.querySelector(".pass-no").style.display = "none";
              document.querySelector(".pass-yes").style.display = "none";
            }


              if( hasClass(helperText.charLength, 'valid') &&
                     hasClass(helperText.lowercase, 'valid') &&
                     hasClass(helperText.uppercase, 'valid') &&
                     hasClass(helperText.special, 'valid')) {

                    $("#reginpx").prop( "disabled", false );
                    $("#reginpx").css("cursor", "pointer");
              }

          }
          else{
            document.querySelector(".pass-yes").style.display = "none";
            document.querySelector(".pass-no").style.display = "block";

            $("#reginpx").prop( "disabled", true );
            $("#reginpx").css("cursor", "not-allowed");
          }
        }

      })();

  </script>
</body>
