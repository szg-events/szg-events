<?php
include_once('classes/class_order.php');
session_start();
include_once("functions.php");

 ?>
 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<title>Product</title>
        <link rel="stylesheet" type="text/css" href="lib/css/style.css">
 	    <link rel="stylesheet" type="text/css" href="lib/css/product.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="lib/js/functions.js"></script>

 </head>
 <body>
 	<?php
 	include("header.php");
 	if(isset($_GET['id']) and $_GET['id'] != NULL){
 		$eventId = htmlspecialchars($_GET['id']);
 		$event = new event($eventId);

 		//create tickets objects
 		$tcktQuery = "SELECT * FROM tickets WHERE event_id = ?";
 		$ticketsRow = getTicketsFromDB($tcktQuery, $eventId);

        $tickets = array();
        $i = 0;
        foreach ($ticketsRow as $row) {
        	$ticket = new ticket($row);
        	$tickets[$i++] = $ticket;
        }
        $imgPath = "images/events/".$event->getImage();
        $desc = $event->getDescription();
        $totalPrice = 0;
        $prices = array();
        $ticketIds = array();
        for ($i=0; $i < count($tickets) ; $i++) {
                $prices[$i] = $tickets[$i]->getPrice();
                $ticketIds[$i] = $tickets[$i]->getId();
       }

       $orderSelected = false;
       if(isset($_SESSION['order'])) {
            $order = new order();
            $order = $_SESSION['order'];
            $orderSelected = true;
        }

 	?>

 	<div id="product">
 		<div id="pleft">
 			<img src="<?= $imgPath ?>">
 			<div id="pdesc"> <?php echo $desc ?>
 			</div>
 		</div>
 		<div id="pright">
 			<div id="headline">Your Order</div>
 			<div class="total">
 				<span id="totaltitle">Total of your order</span>
 				<span id="totalprice"><script>updatePrice(<?php echo json_encode($prices)?>)</script>   </span>
 			</div>
 			<div class="itemwrapper">
 				<form id="prodform" action="product_check.php?order=<?php echo json_encode($ticketIds)?>" method="POST">
 				<div class="item">
 					<?php for ($i=0; $i < count($tickets); $i++) {
                  $selectedAmount = -1;
                  if($orderSelected && $i < count($order->getOrderRows())) {
                      $selectedAmount = $order->getTicketFromIndex($i)[1];
                }?>
 						<div name="ticket" class="informations">
              <div class="infoname">
  		<?php echo $tickets[$i]->getName();?>
              </div>
              <div class="rightbox">
              <div class="infoprice">
              <?php echo $tickets[$i]->getPrice(). " €"; ?>
              </div>
              <?php
    						$amount = $tickets[$i]->getAvailability();
    						if($amount == 0){?>
					         <p class="notAvail">Out of stock</p>
  				      <?php }
                else { ?>
     						<select name="availability<?= $tickets[$i]->getId()?>" class="selamount" class="quantity" tabindex="-1" onchange="updatePrice(<?php echo json_encode($prices)?>)">
    							<option value="0" selected>0</option>
                  <?php
    							for ($j=1; $j <= $amount; $j++) {
                    echo '<option value="'.$j.'">'.$j.'</option>';
    							} ?>
    						</select>
               </div>
	       <?php } ?>
						</div>
				 <?php
 					} ?>
 				</div>
 				<button id="buybutton" disabled="disabled">Add to cart</button>
 				</form>
 			</div>
 		</div>
 	</div>
  <?php }
  else {
    header("Location: 404.php");
  }
?>
  <script type="text/javascript">
  $(document).ready(function() {
    var pos = 0;
    var totalPrice = 0;
    var prices = 0;

    $(".selamount").each(function(pos){
      prices = $('.infoprice')[pos].innerHTML;
      prices = prices.replace(/\s+/g, '');
      prices = prices.replace('€','');
      totalPrice += (prices * $(this).val());
      pos++;
      if($(this).val() > 0){
        $('#buybutton').prop('disabled', false);
      }
    });
    $("#totalprice").text(totalPrice + " €");
  });
  </script>
 </body>
 </html>
