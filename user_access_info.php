<?php
include_once("classes/class_user_access.php");

if (!isset($_SESSION["email_login"])) {
	$_SESSION["redirect"] = "user_access_info.php";
  ?><script>window.location.href='login.php?log=unlog'; </script><?php
  exit();
}

$thisAccess = new UserAccess($_SESSION["email_login"]);
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>test useragent access</title>
	</head>
	<body>
		User Agent: <?php echo $thisAccess->getUserAgent() ?> <br>
		Platform: <?php echo $thisAccess->getPlatformAndVersion() ?> <br>
		Browser: <?php echo $thisAccess->getBrowserAndVersion() ?> <br>
		IP Adress: <?php echo $thisAccess->getIpAddress() ?> <br>
		Region: <?php echo $thisAccess->getRegionAndCountryCode() ?> <br>
		City: <?php echo $thisAccess->getCity() ?> <br>
		<br>
		Token: <?php echo $thisAccess->getDeviceIdentifier(); ?>
	</body>
</html>
