<?php
  session_start();
  include_once("database.php");
  include_once("functions.php");
?>

<?php

  if(isset($_GET['resetid'])){

    $resetid = $_GET['resetid'];

    deleteOldResetId();
    $query_pass = selectResetId($resetid);
    $row_count = mysqli_num_rows($query_pass);

    if($row_count == 1){
      $row = mysqli_fetch_assoc($query_pass);
      $email = $row['email'];
    }
    else if ($row_count == 0){
      echo "<script>alert('Sorry! This password reset link is invalid!');</script>";
      echo "<script>window.location.href='forgot_pass.php';</script>";
    }
    else{
      echo "<script>alert('Sorry! Something went wrong! Try again.');</script>";
      echo "<script>window.location.href='forgot_pass.php';</script>";
    }

  }

  /*check link validity*/

  if (isset($_POST["newPass"]) && isset($_POST["rePass"]))
  {

    $pswd = $_POST["newPass"];
    $pswd = openssl_digest($pswd, 'sha512');
    $pass = array($pswd);

    $res = setNewPassword($email, $pswd);
    if($res){

      $query_del = deleteResetId($resetid);
      echo "<script>alert('Password Changed. Please Login with your new password.');</script>";
      echo "<script>window.location.href='login.php'</script>";

    }
    else{
      printf("Error: Password update failed! %s\n\n", mysqli_error($db));
    }
  }

?>

<head>
  <meta charset="utf-8">
  <title>szg - reset password</title>
  <link rel="stylesheet" type="text/css" href="lib/css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('input[name=newPass], input[name=rePass]').on('keyup', function () {
        var password   = $('input[name=newPass]'),
            repassword = $('input[name=rePass]'),
            both       = password.add(repassword).removeClass('has-success has-error');
        if (password.val().length > 0){
          repassword.addClass(
              password.val().length > 0 ? 'has-success' : 'has-error'
          );

          if (password.val() != repassword.val()) {
              repassword.addClass('has-error');
          } else {
            both.addClass('has-success')
          }
        }
      })
    });
  </script>
</head>
<body>
	<?php include("header.php"); ?>

  <div class="mainContainer resetPass">
    <form class="masterForm passwordChange" action="" method="post" autocomplete="off">
      <input type="email" class="" style="background: whitesmoke; margin-bottom: 10px;" name="usermail" placeholder="Email" value="<?php echo $row['email']; ?>" readonly>
      <input type="password" class="password" name="newPass" placeholder="Type new password">
                <ul class="helper-text">
                    <li class="length">Must be at least 8 characters long.</li>
                    <li class="lowercase">Must contain a lowercase letter.</li>
                    <li class="uppercase">Must contain an uppercase letter.</li>
                    <li class="special">Must contain a special character.</li>
                    <li class="number">Must contain a number.</li>
                </ul>
      <input type="password" class="password2" name="rePass" placeholder="Confirm new password">
              <div class="pass-yes">Yes! Your passwords Match!</div>
              <div class="pass-no">Passwords do not match!</div>
      <input type="submit" name="changePassword" style="margin-top: 20px;" value="Change password">
      <div class="btnCancel" onclick="javascript:window.location='login.php';">Cancel</div>
    </form>
  </div>

  <!-- DYNAMICALLY CHECK IF PASSWORDS MATCH -->

  <script>

    (function(){

      var email = document.querySelector('.email');
      var password = document.querySelector('.password');
      var password2 = document.querySelector('.password2');

      var helperText = {
          charLength: document.querySelector('.helper-text .length'),
          lowercase: document.querySelector('.helper-text .lowercase'),
          uppercase: document.querySelector('.helper-text .uppercase'),
          special: document.querySelector('.helper-text .special'),
          number: document.querySelector('.helper-text .number')
      };

      var pattern = {
        charLength: function() {
            if( password.value.length >= 8 ) {
                return true;
            }
        },
        lowercase: function() {
            var regex = /^(?=.*[a-z]).+$/;

            if( regex.test(password.value) ) {
                return true;
            }
        },
        uppercase: function() {
            var regex = /^(?=.*[A-Z]).+$/;

            if( regex.test(password.value) ) {
                return true;
            }
        },
        special: function() {
            var regex = /^(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]).+$/;

            if( regex.test(password.value) ) {
                return true;
            }
        },
        number: function() {
            var regex = /^(?=.*[0-9]).+$/;

            if( regex.test(password.value) ) {
                return true;
            }
        }
      };

      function patternTest(pattern, response) {
          if(pattern) {
              addClass(response, 'valid');
          } else {
              removeClass(response, 'valid');
          }
      }

      function addClass(el, className) {
          if (el.classList) {
              el.classList.add(className);
          } else {
              el.className += ' ' + className;
          }
      }

      function removeClass(el, className) {
          if (el.classList) {
              el.classList.remove(className);
          } else {
              el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
          }
      }

      function hasClass(el, className) {
          if (el.classList) {
              console.log(el.classList);
              return el.classList.contains(className);
          } else {
              new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className);
          }
      }

      password.addEventListener('click', function (){
        document.querySelector(".helper-text").style.display = "block";
      });

      password.addEventListener('keyup', function (){
        patternTest( pattern.charLength(), helperText.charLength );
        patternTest( pattern.lowercase(), helperText.lowercase );
        patternTest( pattern.uppercase(), helperText.uppercase );
        patternTest( pattern.special(), helperText.special );
        patternTest( pattern.number(), helperText.number );
        validatesub();
      });



      password2.addEventListener('keyup', function (){
        validatesub();
      });

      email.addEventListener('keyup', function (){
            validatesub();
        });

      function validatesub(){

        var regex = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

        if(password.value == password2.value){
          if(password.value != ""){
            document.querySelector(".pass-no").style.display = "none";
            document.querySelector(".pass-yes").style.display = "block";
          }
          else{
            document.querySelector(".pass-no").style.display = "none";
            document.querySelector(".pass-yes").style.display = "none";
          }

          if( regex.test(email.value) ) {

            document.querySelector(".email-no").style.display = "none";
            document.querySelector(".email-yes").style.display = "block";

            if( hasClass(helperText.charLength, 'valid') &&
                   hasClass(helperText.lowercase, 'valid') &&
                   hasClass(helperText.uppercase, 'valid') &&
                   hasClass(helperText.special, 'valid')) {

                  $("#reginp").prop( "disabled", false );
                  $("#reginp").css("cursor", "pointer");
            }
          }
          else{
            document.querySelector(".email-yes").style.display = "none";
            document.querySelector(".email-no").style.display = "block";

            $("#reginp").prop( "disabled", true );
            $("#reginp").css("cursor", "not-allowed");
          }

        }
        else{
          document.querySelector(".pass-yes").style.display = "none";
          document.querySelector(".pass-no").style.display = "block";

          $("#reginp").prop( "disabled", true );
          $("#reginp").css("cursor", "not-allowed");
        }
      }
    })();
  </script>
</body>
