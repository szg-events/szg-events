<?php
ini_set('date.timezone', 'Europe/Berlin');

if(!file_exists('../config.ini')){
    echo 'File ../config.ini does not exist';
    die();
}

$config = parse_ini_file('../config.ini');

$host_name  = "localhost";

$user_name  = "root";
$password   = getenv('DATABASE_PASS');
$database   = "szg_events";

// $user_name  = $config['db_user'];
// $password   = $config['db_pass'];
// $database   = $config['db_name'];

global $db;
$db = mysqli_connect($host_name, $user_name, $password, $database);
$db->set_charset("utf8");
if(!$db)
{
  exit("Connection problem: ".mysql_connect_error());
}

?>
