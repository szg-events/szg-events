<?php
include_once('classes/class_order.php');
session_start();
include_once('classes/class_tickets.php');
include_once('functions.php');
require_once('./config.php');

ini_set('display_errors', 1);
error_reporting(E_ALL);
$order = new order();
if(isset($_SESSION['order'])) { $order = $_SESSION['order']; }
$tickets = array();

$tcktQuery = "SELECT tickets.*, events.name as event_name FROM tickets LEFT JOIN events ON tickets.event_id = events.id WHERE tickets.id = ? LIMIT 1";
$ticketsRow = array();
for ($i=0; $i <count($order->getOrderRows()) ; $i++) {
	$ticketsRow[$i] = getTicketsFromDB($tcktQuery, $order->getTicketFromIndex($i)[0]);

}
$tickets = array();
$prices = array();
$event_name = array();
for ($i=0; $i < count($ticketsRow) ; $i++) {
    $event_name[$i] = $ticketsRow[$i][0]["event_id"];
}
array_multisort($event_name, SORT_ASC, $ticketsRow);

for ($k=0; $k <count($ticketsRow); $k++) {

	for ($j=0; $j <count($ticketsRow[$k]); $j++) {
		$row = $ticketsRow[$k][$j];
		$ticket = new ticket($row);
		$tickets[$k + $j] = $ticket;
		$prices[$k + $j] = $ticket->getPrice();
	}
}
$tickets = orderTickets($order, $tickets);
?>
<!DOCTYPE html>
<html lang="en">
 <head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<title>Your Order</title>
        <link rel="stylesheet" type="text/css" href="lib/css/style.css">
 		<link rel="stylesheet" type="text/css" href="lib/css/cart.css">
 		<link rel="stylesheet" type="text/css" href="lib/css/product.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="lib/js/functions.js"></script>

 </head>
<body>
<?php include("header.php");?>
<div id="order">
	<div id="orleft">
        <div id="headline">Your Order</div>
		<div class="itemwrapper">
 				<form action="product_check.php?order=<?php echo json_encode($ticketIds)?>" method="POST">
 				<div class="item">
                <?php if(!isset($tickets) || count($tickets) == 0 ){
                    ?>
                    <div id="emptyCart"> Your Cart is empty! <br> <a href="index.php">Click here</a> to buy some nice products </div><?php
                }
                else {
 					for ($i=0; $i < count($tickets); $i++) {
 						$selectedAmount = $order->getTicketFromIndex($i)[1];
                        if($i == 0 ||($i > 0 && $tickets[$i]->getEventName() != $tickets[$i-1]->getEventName())){ ?>
                        <div name="Event">
                            <?php echo $tickets[$i]->getEventName();?>
                        </div>
                        <?php }?>
 						<div name="ticket" class="informations">
                            <div class="infoname">
        					   <?php echo $tickets[$i]->getName();?>
                            </div>
                            <div class="rightbox">
                            <div class="infoprice">
                                <?php
                                   echo $tickets[$i]->getPrice() . " € &nbsp; x"; ?>
                                </div>
                        <?php
                			$amount = $tickets[$i]->getAvailability();
                						//echo $amount;
    						if($amount == 0){?>
						         <p class="notAvail">Out of stock</p>
    				              <?php }
    						else {
                                $selectName = "availability".$tickets[$i]->getId();?>
         						<select name="<?= $selectName?>" id="<?= $selectName?>" class="selamount" autocomplete="off" class="quantity" tabindex="-1" onchange="updateCart(<?= $tickets[$i]->getId()?>, <?php echo json_encode($prices) ?>, <?= $tickets[$i]->getAvailability()?>)" >
        							<?php
        							for ($j=1; $j <= $amount; $j++) {
        								if($j == $selectedAmount){
        									echo '<option value="'.$j.'" selected>'.$j.'</option>';
        								}
        								else {
        									echo '<option value="'.$j.'">'.$j.'</option>';
        								}
        							}
        							?>
        						</select>

                                <div class="cart-content">
                                    <input class="removeFromCart" type="submit" name="removeproduct" value="Remove" onclick="removeProductFromCart(<?=$tickets[$i]->getId()?>), refresh()">
                                </div>
                	 <?php } ?>
                            </div>

						</div>
				 <?php
 					} ?>
                <?php } ?>
                </div>
 				</form>
 			</div>
	</div>
	<div id="orright">
 			<div class="total">
 				<span id="totaltitle">Total of your order</span>
 				<span id="totalprice">
                <script>updatePrice(<?php echo json_encode($prices)?>);</script></span>
 			</div>

            <script type='text/javascript'>
                (function(){
                    if( window.localStorage ){
                      if( !localStorage.getItem('firstLoad') ){
                        localStorage['firstLoad'] = true;
                        window.location.reload();
                      }
                      else
                        localStorage.removeItem('firstLoad');
                    }
                })();
            </script>


            <?php
                if(isset($_COOKIE['tot'])){
                    $tot =  $_COOKIE['tot']*100;
                }else{$tot = 0;}

                if($tot > 0){

                    if(!isset($_SESSION['email_login'])){
                    ?>
                            <div class="paybutton" onclick="window.location='login.php?active=cart'">
                            </div>
                    <?php
                    }
                    else if(isset($_SESSION['email_login'])){
                        $data = getUserDataByMail($_SESSION['email_login']);
                        $tid = $data['id'];
                        ?>

                        <div class="paybut" style="">
                        <form action="charge.php?amount=<?php echo $tot; ?>&&trans=<?php echo $tid; ?>" method="post">
                          <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                  data-key="<?php echo $stripe['publishable_key']; ?>"
                                  data-amount="<?php echo $tot; ?>" data-description="Szg-Events"></script>
                        </form>
                        </div>

                        <?php
                    }
                }
            ?>
	</div>
</div>
</body>
</html>
