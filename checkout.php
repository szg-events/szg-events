<?php 
session_start();
if (!isset($_SESSION["email_login"])) {
  ?><script>window.location.href='login.php?log=unlog&&source=checkout'; </script><?php

  exit();
}
?>

<html>
<head>
	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<title>Checkout</title>
 	<link rel="stylesheet" type="text/css" href="lib/css/style.css">
    <link rel="stylesheet" type="text/css" href="lib/css/checkout.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="lib/js/functions.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div id="wrapper">
	<div id="payblock">
		Pay with...
		<div class="paymethod" onclick="window.location='order_to_db.php'">
			Paypal
		</div>
		<div class="paymethod" onclick="window.location='order_to_db.php'">
			Credit Card
		</div>
		<div class="paymethod" onclick="window.location='order_to_db.php'">
			Bank Transfer
		</div>
		<div>
			<?php if(isset($_SESSION['errorAmount'])){
				$_SESSION['errorAmount'];
				unset($_SESSION['errorAmount']);
			} ?>
		</div>
		<div id="BackButton" onclick="window.history.back();">
			Go Back
		</div>
	</div>
</div>
</body>
</html>
