<?php
session_start();
include_once("database.php");
include_once("functions.php");

if(isset($_POST['sendlink'])){
	$email = $_POST['email'];
	$query_res = getUserDataByEmail($email);
	$row_count = mysqli_num_rows($query_res);


	if($row_count == 1){
		$resetid = generateRandomString();
		$timestamp = time();

		$query_ins = insertResetId($email, $resetid, $timestamp);

		if($query_ins){
			$configMail = parse_ini_file('../config.ini');
			$address = "https://ec2-54-213-248-219.us-west-2.compute.amazonaws.com"; //Website name or localhost address

			$to = $email;
			$subject = "SzG Events - Lets reset that password!";
			$txt = "
			Hi,\n\n
					We just received a reset password request for your account at SzG events.\n\n
					If this was you please click the link below to reset the password.\n
					Or copy and paste it on your browser!\n\n\n
					".$address."/reset_pass.php?resetid=".$resetid."&&change=yes\n\n\n
					If this was not you, please ignore this email. Your password is safe. And it will not be changed.\n\n
					NOTE: This link expires in one hour.\n\n
			Have a good day,\n
			your team at SzG Events.";
			$headers = "From: ".$configMail['mail']."\r\n";

			if(mail($to,$subject,$txt,$headers)) {
				echo '<script>alert("You should receive a mail shortly!");</script>';
			} else {
				echo '<script>alert("Some error occurred!");</script>';
			}
		}
		else{
			printf("Error: Recording the resetid in database failed. %s\n", mysqli_error($db));
		}

	}
	else if ($row_count == 0){
		echo '<script>alert("Sorry! This email is not registered with us!");</script>';
	}
	else{
		echo '<script>alert("Sorry! Our bad. Something went wrong!");</script>';
	}


	// PLEASE UPDATE YOUR DATABASE SQL FILE FOR THIS TO WORK
	//gmail = events.szg@gmail.com
	//pass = Events1234!
}

?>

<head>
  <meta charset="utf-8">
  <title>szg - reset password</title>
  <link rel="stylesheet" type="text/css" href="lib/css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body>
<?php include('header.php'); ?>
<br/>
<br/>
<div class="mainContainer resetPass">
    <form class="masterForm passwordChange" action="" method="post" autocomplete="off">
      <input type="email" name="email" class="email" placeholder="Your account email">
      		<div class="email-yes">Hmm, looks like a valid email!</div>
			<div class="email-no">Please enter a valid email</div>
      <input type="email" name="remail" class="remail" placeholder="Confirm your email">
      		<div class="pass-yes">Yes! Your emails Match!</div>
			<div class="pass-no">Emails do not match!</div>
      <input type="submit" name="sendlink" id="reginp" style="margin-top: 20px;" value="Submit">
    </form>
</div>

<div>
	<p id="info"> You will receive a email link to reset your password. </p>
</div>


<script type="text/javascript">

	(function(){

			var email = document.querySelector('.email');
			var remail = document.querySelector('.remail');

			remail.addEventListener('keyup', function (){
				validatesub();
			});

			email.addEventListener('keyup', function (){
        		validatesub();
		    });

			function validatesub(){

				var regex = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

				if( regex.test(email.value) ) {
						document.querySelector(".email-no").style.display = "none";
						document.querySelector(".email-yes").style.display = "block";


						if(email.value == remail.value){

							if(email.value != ""){
								document.querySelector(".pass-no").style.display = "none";
								document.querySelector(".pass-yes").style.display = "block";

								$("#reginp").prop( "disabled", false );
								$("#reginp").css("cursor", "pointer");
							}
							else{
								document.querySelector(".email-yes").style.display = "none";
								document.querySelector(".email-no").style.display = "block";

								$("#reginp").prop( "disabled", true );
								$("#reginp").css("cursor", "not-allowed");

							}
						}
						else{
							document.querySelector(".pass-yes").style.display = "none";
							document.querySelector(".pass-no").style.display = "block";

							$("#reginp").prop( "disabled", true );
							$("#reginp").css("cursor", "not-allowed");
						}

				}
				else{
						document.querySelector(".email-yes").style.display = "none";
						document.querySelector(".email-no").style.display = "block";

						$("#reginp").prop( "disabled", true );
						$("#reginp").css("cursor", "not-allowed");
				}

			}

	})();


</script>
</body>
