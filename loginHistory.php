<?php
$navbarTitle = "Login History";

session_start();
if (!isset($_SESSION["email_login"])) {
  $_SESSION["redirect"] = "loginHistory.php";
  header("Location: login.php?log=unlog");
  exit();
}
include_once("functions.php");

$email = $_SESSION["email_login"];
$devices = getDevicesList($email);

?>

<head>
  <meta charset="utf-8">
  <title>SzG - Login History</title>
  <link rel="stylesheet" type="text/css" href="lib/css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>
<body>
	<?php include("header.php"); ?>

  <div class="eventlist">
    <div class="eventbox deviceBox bold">
      <div class="deviceRowContent">Operating system</div>
      <div class="deviceRowContent">Browser</div>
      <div class="deviceRowContent">Region</div>
      <div class="deviceRowContent">Last used</div>
    </div>

    <div class="eventbox deviceBox">
    <?php
    foreach ($devices as $id => $device) {
      ?>
        <?php $identifier = explode("&&", $device["identifier"]); ?>
        <div class="deviceRowContent"><?= $identifier[0] ?></div>
        <div class="deviceRowContent"><?= $identifier[1] ?></div>
        <div class="deviceRowContent"><?= $identifier[2] ?></div>
        <div class="deviceRowContent"><?= $device["last_login"] ?></div>
      <?php
    }?>
    </div>
  </div>
</body>
