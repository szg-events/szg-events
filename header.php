<!-- THE HEADER FILE for the Base TEMPLATE-->
<html>

<?php if (!isset($navbarTitle)) { $navbarTitle = "SzG-Events"; } ?>

		<div class="headernav">
			<div class="headerlogo" onclick="location.href='index.php'"><img src="images/logo-min.jpg" style="width: 100%;"></div>
			<div class="headertitle" onclick="location.href='index.php'"><?= $navbarTitle ?></div>
			<div class="navlist">

				<a href="index.php"><div class="nav">HOME</div></a>
				<a href="cart.php"><div class="nav">CART</div></a>

				<!-- if user is LOGGED OUT of the account (SHOW LOGIN)-->
				<?php if(!isset($_SESSION["email_login"])) { ?>
					<a href="login.php"><div class="nav">LOGIN</div></a>

				<!-- if user is LOGGED INTO the account (SHOW Personal pages and LOGOUT)-->
				<?php } else if(isset($_SESSION["email_login"])) { ?>
					<a href="myTickets.php"><div class="nav">MY TICKETS</div></a>
					<a href="settings.php"><div class="nav">SETTINGS</div></a>
					<a href="logout.php"><div class="nav" id="logout">LOGOUT</div></a>
				<?php } ?>
			</div>
		</div>
