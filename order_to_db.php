<?php
include_once("classes/class_order.php");
session_start();
include_once("functions.php");
include_once("classes/class_tickets.php");

ini_set('display_errors', 1);
error_reporting(E_ALL);

$email = $_SESSION["email_login"];
$info = array();
$info = getUserDataByMail($email);
$userId = $info["id"];

if(isset($_SESSION['order'])){
		$order = $_SESSION['order'];
    createOrderFromCart($userId, $order->getOrderRows());
    unset($_SESSION['order']);
}
else{
	header('location: charge.php');
}
?>
