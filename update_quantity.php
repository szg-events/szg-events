<?php

include_once("classes/class_order.php");
include_once("classes/class_tickets.php");

session_start();

$order = new order();
if(isset($_SESSION['order']) && isset($_POST['qnty'])){
	$order = $_SESSION['order'];
	$qnty = $_POST['qnty'];
	$ticketId = json_decode(htmlspecialchars($_GET['prod']));
	if($qnty > 0 && $qnty<= getTicketAmountById($ticketId)){
		$order->addCouple($ticketId, $qnty);
	}

	$_SESSION['order'] = $order;
	header('Refresh:0; url=cart.php');
}
else{
	header('location: error.php');
}
 ?>}
