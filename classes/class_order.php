<?php
include_once("class_tickets.php");
include_once("functions.php");

class order {
	private $orderRows = array();
	function __construct(){}

	function addCouple($ticket, $amount){
		$couple = array($ticket, $amount);
		$this->removeCouple($ticket);
		array_push($this->orderRows, $couple);
	}

	//getters and setters
	public function getOrderRows(){return $this->orderRows;}

	function getTicketFromIndex($pos){return $this->orderRows[$pos];}

	function removeCouple($ticketId){
		$i = 0;
		$exists = false;
		while($exists == false && $i < count($this->orderRows)){
			if($this->orderRows[$i][0] == $ticketId){
				$exists = true;
				//delete the couple and reindex the orderRows
				unset($this->orderRows[$i]);
				$this->toString();
				$this->orderRows = array_values($this->orderRows);
				echo "after: " . $this->toString();
			}
			$i++;
		}
	}

	function toString(){
		$str = "";
		for ($i=0; $i < count($this->orderRows); $i++) {
			$str .= 'couple ' .$i. ': [' .$this->orderRows[$i][0]. ', ' .$this->orderRows[$i][1]. ']<br>';
		}
		echo $str;
	}
}
?>
