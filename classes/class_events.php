<?php
include_once("functions.php");
class event {
	var $id = "";
	var $name = "";
	var $startdate = "";
	var $enddate = "";
	var $location = "";
	var $desc = "";
	var $img = "";
	function __construct($bind){
		$array = getObjectFromDB("SELECT * FROM events WHERE id=? LIMIT 1", $bind);
		if(!$array){
			header("Location: 404.php");
		}
		$this->id = $array["id"];
		$this->name = $array["name"];
		$this->startdate = $array["startdate"];
		$this->enddate = $array["enddate"];
		$this->location = $array["location"];
		$this->desc = $array["description"];
		$this->img = $array["image"];
	}


	function getId(){return $this->id;}
	function getStartDate(){return $this->startdate;}
	function getName(){return $this->name;}
	function getEndDate(){return $this->enddate;}
	function getLocation(){return $this->location;}
	function getDescription(){return $this->desc;}
	function getImage(){return $this->img;}


	function toString(){
		$str = $this->id. "<br> " .$this->name. "<br> " .$this->startdate. "<br> " .$this->enddate. "<br> " .$this->location. "<br> " .$this->desc. "<br> " .$this->img;
		echo $str;
	}
	} ?>
