<?php
include_once("functions.php");

// query
// SELECT events.id, events.name, events.startdate, events.enddate, events.image, tickets.id, tickets.name, order_details.amount
// 		FROM orders INNER JOIN order_details on orders.id = order_details.order_id
// 								INNER JOIN tickets on order_details.ticket_id = tickets.id
// 								INNER JOIN events on tickets.event_id = events.id

/**
 * Represents all information that is needed to display an event a user purchased tickets for
 */
class PurchasedEvent {

	private $id;
	private $name;
	private $startdate;
	private $enddate;
	private $image;
	private $tickets = array();

	function getId(){return $this->id;}
	function getName(){return $this->name;}
	function getStartDate(){return $this->startdate;}
	function getEndDate(){return $this->enddate;}
	function getImage(){return $this->image;}
	function getTickets(){return $this->tickets;}

	function __construct($params) {
		$this->id = $params["eventId"];
		$this->name = $params["eventName"];
		$this->startdate = $params["startdate"];
		$this->enddate = $params["enddate"];
		$this->image = $params["image"];
	}
	public static function withParamsArray($params) {
        $instance = new self($params);
        return $instance;
  }

	public static function withValues($eventId, $eventName, $startdate, $enddate, $image) {
		$instance = new self(array('eventId' => $eventId,
															 'eventName' => $eventName,
															 'startdate' => $startdate,
															 'enddate' => $enddate,
															 'image' => $image));
		return $instance;
	}

	public function addPurchasedTicket($ticketId, $ticketName, $price_eur, $quantity) {
		$t = new PurchasedTicket($ticketId, $ticketName, $price_eur, $quantity);
		$this->tickets[$ticketId] = $t;
	}
}

/**
 * Represents all information that is needed to display an purchased ticket
 */
class PurchasedTicket {
	private $id;
	private $name;
	private $price;
	private $quantity;

	function getId(){return $this->id;}
	function getName(){return $this->name;}
	function getPrice(){return $this->price;}
	function getQuantity(){return $this->quantity;}

	function __construct($ticketId, $ticketName, $price_eur, $quantity) {
			$this->id = $ticketId;
			$this->name = $ticketName;
			$this->price = $price_eur;
			$this->quantity = $quantity;
	}
}



?>
