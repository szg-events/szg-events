<?php
include_once("classes/class_events.php");
include_once("functions.php");
class ticket {
	var $id = "";
	var $eventId = "";
	var $name = "";
	var $price = "";
	var $avail = "";
	var $event_name = null;

	function __construct($array){
		$this->id = $array["id"];
		$this->name = $array["name"];
		$this->eventId = $array["event_id"];
		$this->price = $array["price_eur"];
		$this->avail = $array["available_amount"];
		if(isset($array["event_name"])){
			$this->event_name = $array["event_name"];
		}
		else {
			$this->event_name = null;
		}
	}

	//getters and setters
	function setId($newId){$this->id = $newId;}
	function setName($newName){$this->name = $newName;}
	function setPrice($newPrice){$this->price = $newPrice;}
	function setAvailability($newAvail){$this->avail = $newAvail;}

	function getId(){return $this->id;}
	function getEvent(){return $this->eventId;}
	function getName(){return $this->name;}
	function getPrice(){return $this->price;}
	function getAvailability(){return $this->avail;}
	function getEventName(){return $this->event_name;}

	function toString(){
		$str = $this->id. "<br> " .$this->name. "<br> " .$this->price. "<br> " .$this->avail. "<br> ";
		echo $str;
	}
}
?>
