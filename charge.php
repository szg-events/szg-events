<?php

  include('functions.php');
  require_once('./config.php');

  if(isset($_GET['amount'])){
    $tot_amount = $_GET['amount'];
  } else{$tot_amount = 0;}

  if(isset($_GET['trans'])){
    $result = getUserDataById($_GET['trans']);
    $em = $result['email'];
  } else{$em= "";}

  $token  = $_POST['stripeToken'];
  $customer = \Stripe\Customer::create(array(
      'email' => $em,
      'card'  => $token
  ));
  $charge = \Stripe\Charge::create(array(
      'customer' => $customer->id,
      'amount'   => $tot_amount,
      'currency' => 'eur'
  ));

  // call order_to_db.php
  include ('order_to_db.php');
?>

<head>
<link rel="stylesheet" type="text/css" href="lib/css/style.css">  
</head>

<body>
  <div class="receiptbox">
    <?php
    echo '<h1>Successfully charged!</h1><p>----</p><br/>';
    echo '<br/>email:'.$customer->email;
    echo '<br/>amount:'.($charge->amount)/100;
    echo '<br/>currency:'.$charge->currency;  
    ?>
    <div onclick="window.location='myTickets.php'" class="simpbut">
      Proceed to My Tickets
    </div>
  </div>

</body>