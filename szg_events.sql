-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 08, 2017 at 12:22 AM
-- Server version: 5.7.16
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `szg_events`
--

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `identifier` VARCHAR(100) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, 
  `blocked` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `startdate` timestamp NOT NULL,
  `enddate` timestamp NULL DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `image` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `startdate`, `enddate`, `location`, `description`, `image`) VALUES
(1, 'Streetparade 2017', '2017-08-12 05:00:00', '2017-08-17 00:00:00', 'Zürich (Switzerland)', 'Streetparade Zürich\r\nbabedibubi', '2017_streetparade.jpg'),
(2, 'Electric Love Festival 2017', '2017-07-05 08:00:00', '2017-07-09 12:00:00', 'Salzburg (Austria)', 'Electric Love Festival\r\nbabedibubi', '2017_electric_love.jpg'),
(3, 'Streetparade 2016', '2016-08-13 05:00:00', '2017-08-17 00:00:00', 'Zürich (Switzerland)', 'Streetparade Zürich\r\nbabedibubi', '2016_streetparade.jpg'),
(4, 'Lake Festival 2017', '2017-08-09 07:00:00', '2017-08-13 12:00:00', 'Graz (Austria)', 'Lake Festival am Schwarzlsee\r\nbabedibubi', '2017_lake_festival.jpg'),
(5, 'Shutdown Festival 2017', '2017-08-26 10:00:00', '2017-08-31 00:00:00', 'Zwentendorf (Austria)', 'Shutdown Festival - HARDSTYLE\r\nbabedibubi', '2017_shutdown.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `datetime`) VALUES
(1, 20, '2017-06-07 10:59:24'),
(2, 20, '2017-06-07 11:04:55'),
(3, 20, '2017-06-07 11:07:39');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `ticket_id` bigint(20) UNSIGNED NOT NULL,
  `amount` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`order_id`, `ticket_id`, `amount`) VALUES
(1, 1, 10),
(2, 1, 10),
(2, 2, 1),
(2, 3, 1),
(3, 2, 1),
(3, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `passchange`
--

CREATE TABLE `passchange` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `resetid` varchar(100) NOT NULL,
  `gen_time` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `event_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `price_eur` float NOT NULL,
  `available_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `event_id`, `name`, `price_eur`, `available_amount`) VALUES
(1, 1, 'Bus', 60, 30),
(2, 2, 'Bus', 70, 48),
(3, 2, 'Ticket', 130, 48),
(4, 2, 'VIP - Ticket', 200, 50),
(5, 3, 'Bus', 55, 50),
(6, 4, 'Bus', 80, 50),
(7, 4, 'Ticket', 130, 50),
(8, 4, 'VIP - Ticket', 200, 50),
(9, 5, 'Bus', 80, 50),
(10, 5, 'Ticket', 130, 50);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `password` varchar(2000) NOT NULL,
  `logcount` int(10) DEFAULT '0',
  `actid` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `firstname`, `lastname`, `password`, `logcount`, `actid`) VALUES
(3, 'anjan8@gmssail.com', 'Anjan', 'Karmakar', '25d55ad283aa400af464c76d713c07ad', NULL, NULL),
(4, 'root@x.co', 'Anjan', 'Karmakar', '1adbb3178591fd5bb0c248518f39bf6d', NULL, NULL),
(6, 'anjan238@gmail.com', 'Anjan', 'Karmakar', '828fd9255753432d51df95eb62d61722', NULL, NULL),
(7, 'anjanasa8@gmail.com', 'Anjan', 'Karmakar', '62c8ad0a15d9d1ca38d5dee762a16e01', NULL, NULL),
(8, 'anjan82@gmail.com', 'Anjan', 'Karmakar', '25d55ad283aa400af464c76d713c07ad', NULL, NULL),
(9, 'anjan8@gmail.comxxx', 'Anjan', 'Karmakar', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', NULL, NULL),
(10, 'a@x.com', 'Bob', 'Green', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', NULL, NULL),
(11, 'anjan@tree.com', 'Anjan', 'Karmakar', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', NULL, NULL),
(15, 'anjan.ms@live.comm', 'Anjan', 'Karmakar', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', NULL, NULL),
(16, 'anjan.ms@live.comx', 'Anjan', 'Karmakar', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', NULL, NULL),
(17, 'anjan.ms@live.com', 'Anjan', 'Karmakar', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', NULL, NULL),
(18, 'gabminne@hotmail.it', 'Gabriele', 'Minneci', 'c3a31051a239499708c1b8f60aad25d85722d6faec17646e2260ff3f80179bf77acde339448e23d27b3aa7cb889d528fee2aef60cb87d36a87a91abdf9dac9cb', NULL, NULL),
(19, 'anjan.ms@live.commx', 'Anjan', 'Karmakar', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', 3, NULL),
(20, 'anjan8@gmail.com', 'Anjan', 'Karmakar', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', 0, ''),
(21, 'anj@x.com', 'Anjan', 'Karmakar', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', 0, NULL),
(22, 'an@x.com', 'Anjan', 'Karij9', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', 0, NULL),
(23, 'a@b.xom', 'A', 'K', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', 0, NULL),
(24, 'a@x.omc', 'A', 'D', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', 0, NULL),
(25, 'A@x.xom', 'A', 'S', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', 0, NULL),
(26, 'a@lklk.com', 'A', 'B', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', 0, NULL),
(27, 'a@x.comahsahsa', 'A', 'C', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', 0, NULL),
(28, 'a@c.com', 'A', 'B', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', 0, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`order_id`,`ticket_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `ticket_id` (`ticket_id`);

--
-- Indexes for table `passchange`
--
ALTER TABLE `passchange`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `event_id` (`event_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `passchange`
--
ALTER TABLE `passchange`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `devices`
--
ALTER TABLE `devices`
  ADD CONSTRAINT `user_device` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `user_orders` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_order_details` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ticket_order_details` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `event_tickets` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
