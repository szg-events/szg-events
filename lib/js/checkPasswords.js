$(document).ready(function(){
  $('input[name=newPass], input[name=rePass]').on('keyup', function () {
    var password   = $('input[name=newPass]'),
        repassword = $('input[name=rePass]'),
        both       = password.add(repassword).removeClass('has-success has-error');
    if (password.val().length > 0){
      repassword.addClass(
          password.val().length > 0 ? 'has-success' : 'has-error'
      );

      if (password.val() != repassword.val()) {
          repassword.addClass('has-error');
      } else {
        both.addClass('has-success')
      }
    }
  })
});



