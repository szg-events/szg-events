function updatePrice(prices){
                var pos = 0;
                var totalPrice = 0;
                $(".selamount").each(function(pos){
                    totalPrice += (prices[pos] * $(this).val());
                    pos++;
                    if($(this).val() > 0){
                      $('#buybutton').prop('disabled', false);
                    }
                });
               $("#totalprice").text(totalPrice + " €");
               document.cookie = "tot="+ totalPrice;
}

function removeProductFromCart(id) {
    if(confirm('Do you really want to remove this product from your cart?')){
        $.ajax({ 
            url: "remove.php?prod=" + id          
        });
    }
    else {
        return;
    }
}

function refresh(){
    setTimeout(function () {
        window.location.reload();
    }, 100); 
}

function updateQuantity(id, stock) {
    var qnty = document.getElementById('availability'+id).value;
    //console.log(qnty);
    if (qnty > stock) {
        alert("This product is only " + stock + "times available. Please choose another amount.");
    }
    else {
        $.ajax({ url: "update_quantity.php?prod="+id, type: "POST", data: { 'qnty': qnty }, success: function() {
            window.location.reload();
            }
        });
    }
}

function updateCart(id, prices, stock){
    updatePrice(prices);
    updateQuantity(id, stock);
}

// function checkNotEmpty(){
//     var check = false;
//     $(".selamount").each(function(pos){
//         if($(this).val() != 0){
//             check = true;
//         }
//     });
//     if(check){
//         $("#prodform").submit();
//     }
//     else{
//         $('#buybutton').prop('disabled', true);
//         alert("Please select the quantity of the tickets you want to buy.");
//     }
// }
